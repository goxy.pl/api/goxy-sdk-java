package pl.goxy.sdk.exception;

import pl.goxy.sdk.model.response.ErrorResponse;

public class GoxyApiErrorException
        extends Exception
{
    private final ErrorResponse error;
    private final int statusCode;
    private final String message;

    public GoxyApiErrorException(ErrorResponse error, int statusCode, String message)
    {
        this.error = error;
        this.statusCode = statusCode;
        this.message = message;
    }

    public ErrorResponse getError()
    {
        return this.error;
    }

    public int getStatusCode()
    {
        return this.statusCode;
    }

    public int getCode()
    {
        return this.error.getCode();
    }

    public String getRawMessage()
    {
        return this.message;
    }

    @Override
    public String getMessage()
    {
        return String.format("%s (%s)", this.message, this.getCode());
    }
}
