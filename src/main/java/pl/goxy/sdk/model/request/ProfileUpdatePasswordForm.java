package pl.goxy.sdk.model.request;

import org.jetbrains.annotations.Nullable;

public class ProfileUpdatePasswordForm {
    @Nullable
    private final String password;

    public ProfileUpdatePasswordForm(@Nullable String password) {
        this.password = password;
    }

    @Nullable
    public String getPassword() {
        return password;
    }
}
