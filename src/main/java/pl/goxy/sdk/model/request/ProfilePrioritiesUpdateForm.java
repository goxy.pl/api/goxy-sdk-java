package pl.goxy.sdk.model.request;

import java.util.Collection;

public class ProfilePrioritiesUpdateForm {
    private final Collection<ProfilePriorityEntry> entries;

    public ProfilePrioritiesUpdateForm(Collection<ProfilePriorityEntry> entries) {
        this.entries = entries;
    }

    public Collection<ProfilePriorityEntry> getEntries() {
        return entries;
    }
}
