package pl.goxy.sdk.model.request;

public class ServerCreateLegacyRequest {
    private final int port;
    private final int maxPlayers;

    public ServerCreateLegacyRequest(int port, int maxPlayers) {
        this.port = port;
        this.maxPlayers = maxPlayers;
    }

    public int getPort() {
        return port;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }
}
