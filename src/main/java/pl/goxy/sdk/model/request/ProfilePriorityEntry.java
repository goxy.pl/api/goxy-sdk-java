package pl.goxy.sdk.model.request;

import java.util.UUID;

public class ProfilePriorityEntry {
    private final UUID id;
    private final int priority;

    public ProfilePriorityEntry(UUID id, int priority) {
        this.id = id;
        this.priority = priority;
    }

    public UUID getId() {
        return id;
    }

    public int getPriority() {
        return priority;
    }
}
