package pl.goxy.sdk.model.request;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public class ServerCreateRequest {
    @NotNull
    private final String name;

    @NotNull
    private final String address;
    private final int maxPlayers;

    @Nullable
    private final UUID containerId;

    public ServerCreateRequest(@NotNull String name, @NotNull String address, int maxPlayers, @Nullable UUID containerId) {
        this.name = name;
        this.address = address;
        this.maxPlayers = maxPlayers;
        this.containerId = containerId;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public String getAddress() {
        return address;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    @Nullable
    public UUID getContainerId() {
        return containerId;
    }
}
