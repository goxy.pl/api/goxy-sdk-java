package pl.goxy.sdk.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenResponse {
    private TokenType type;
    private TokenAccess access;

    private ServerResponse server;

    private TokenPermissions permissions;

    public TokenType getType() {
        return type;
    }

    public TokenAccess getAccess() {
        return access;
    }

    public ServerResponse getServer() {
        return server;
    }

    public TokenPermissions getPermissions() {
        return permissions;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TokenResponse.class.getSimpleName() + "[", "]").add("type=" + type)
                .add("access=" + access)
                .add("server=" + server)
                .add("permissions=" + permissions)
                .toString();
    }

    public enum TokenType {
        @JsonProperty("proxy") PROXY("Proxy"),

        @JsonProperty("network") SERVER("Server"),

        @JsonProperty("client") CLIENT("Bearer");

        private final String type;

        TokenType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public enum TokenAccess {
        SERVER,
        CONTAINER,
        NETWORK;
    }
}
