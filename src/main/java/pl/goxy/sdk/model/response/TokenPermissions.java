package pl.goxy.sdk.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenPermissions {
    private boolean allowProfilesManagement;
    private boolean allowPermissionsSynchronization;

    public boolean isAllowProfilesManagement() {
        return allowProfilesManagement;
    }

    public boolean isAllowPermissionsSynchronization() {
        return allowPermissionsSynchronization;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", TokenPermissions.class.getSimpleName() + "[", "]")
                .add("allowProfilesManagement=" + allowProfilesManagement)
                .add("allowPermissionsSynchronization=" + allowPermissionsSynchronization)
                .toString();
    }
}
