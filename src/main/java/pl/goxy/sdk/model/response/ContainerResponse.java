package pl.goxy.sdk.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContainerResponse
{
    private UUID id;

    private String name;

    private final List<ServerResponse> servers = new ArrayList<>();

    public UUID getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public List<ServerResponse> getServers()
    {
        return servers;
    }
}
