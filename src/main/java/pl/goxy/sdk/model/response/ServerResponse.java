package pl.goxy.sdk.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.Duration;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.StringJoiner;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServerResponse
{
    private UUID id;

    private String name;

    private int onlinePlayers;

    private int maxPlayers;

    private String token;

    private ServerState state;

    public UUID getId()
    {
        return id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getOnlinePlayers()
    {
        return onlinePlayers;
    }

    public void setOnlinePlayers(int onlinePlayers)
    {
        this.onlinePlayers = onlinePlayers;
    }

    public int getMaxPlayers()
    {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers)
    {
        this.maxPlayers = maxPlayers;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public ServerState getState() {
        return state;
    }

    public void setState(ServerState state) {
        this.state = state;
    }

    public boolean isOnline(ZonedDateTime now, Duration duration) {
        return this.state != null && this.state.getLastConnection() != null && now.minus(duration).isBefore(this.state.getLastConnection());
    }

    public boolean isOnline() {
        return this.isOnline(ZonedDateTime.now(ZoneOffset.UTC), Duration.ofSeconds(30));
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", ServerResponse.class.getSimpleName() + "[", "]").add("id=" + id).toString();
    }
}
