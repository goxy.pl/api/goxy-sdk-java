package pl.goxy.sdk.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.ZonedDateTime;
import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServerState
{
    private ZonedDateTime lastConnection;
    private String pluginVersion;

    public ZonedDateTime getLastConnection() {
        return lastConnection;
    }

    public void setLastConnection(ZonedDateTime lastConnection) {
        this.lastConnection = lastConnection;
    }

    public String getPluginVersion() {
        return pluginVersion;
    }

    public void setPluginVersion(String pluginVersion) {
        this.pluginVersion = pluginVersion;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ServerState.class.getSimpleName() + "[", "]")
                .add("lastConnection=" + lastConnection)
                .add("pluginVersion='" + pluginVersion + "'")
                .toString();
    }
}
