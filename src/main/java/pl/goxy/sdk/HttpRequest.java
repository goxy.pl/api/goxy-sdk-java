package pl.goxy.sdk;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import pl.goxy.sdk.exception.GoxyApiErrorException;
import pl.goxy.sdk.model.response.ErrorResponse;
import pl.goxy.sdk.model.response.TokenResponse.TokenType;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class HttpRequest
{
    private static final ErrorResponse UNEXPECTED_ERROR = new ErrorResponse(-1, "UNEXPECTED_ERROR", "Unexpected Error");
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    private final ObjectMapper objectMapper;
    private final OkHttpClient client;

    private final String baseUrl;
    private final TokenType tokenType;
    private final String token;
    private final GoxyProperties properties;

    public HttpRequest(String baseUrl, TokenType tokenType, String token, GoxyProperties properties)
    {
        this.objectMapper = new ObjectMapper();
        this.objectMapper.registerModule(new JavaTimeModule());
        this.client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .callTimeout(5, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(5, 30, TimeUnit.SECONDS))
                .build();

        this.baseUrl = baseUrl;
        this.tokenType = tokenType;
        this.token = token;
        this.properties = properties;
    }

    public <T> T get(String path, Type type) throws IOException, GoxyApiErrorException
    {
        Request request = this.buildRequest().url(this.baseUrl + path).build();
        return this.call(request, type);
    }

    private Request.Builder buildRequest()
    {
        String userAgent = String.format("%s %s", this.properties.getName(), this.properties.getVersion());
        String authorization = String.format("%s %s", this.tokenType.getType(), this.token);
        return new Request.Builder().addHeader("Authorization", authorization)
                .addHeader("User-Agent", userAgent)
                .addHeader("Accept", "application/json");
    }

    private <T> T call(Request request, @NotNull Type type) throws IOException, GoxyApiErrorException
    {
        Objects.requireNonNull(type, "type");
        try (Response response = this.client.newCall(request).execute())
        {
            ResponseBody body = response.body();
            int statusCode = response.code();
            if (body == null)
            {
                throw new GoxyApiErrorException(UNEXPECTED_ERROR, statusCode, "Unexpected error");
            }
            String bodyString = body.string();
            if (statusCode == 204)
            {
                return null;
            }
            if (!response.isSuccessful())
            {
                try
                {
                    ErrorResponse error = this.objectMapper.readValue(bodyString, ErrorResponse.class);
                    if (error != null)
                    {
                        String message = error.getMessage() + " (" + error.getStatus() + ")";
                        throw new GoxyApiErrorException(error, statusCode, message);
                    }
                }
                catch (JsonParseException | MismatchedInputException ignored)
                {
                }
                throw new GoxyApiErrorException(UNEXPECTED_ERROR, statusCode,
                        "Unexpected error with status code " + statusCode);
            }
            if (type.equals(Void.class)) {
                return null;
            }
            return this.objectMapper.readValue(bodyString, this.objectMapper.constructType(type));
        }
    }

    public <T> T get(String path, Map<String, String> queryParams, Type type) throws IOException, GoxyApiErrorException
    {
        HttpUrl.Builder urlBuilder = HttpUrl.get(this.baseUrl + path).newBuilder();
        queryParams.forEach(urlBuilder::addQueryParameter);

        Request request = this.buildRequest().url(urlBuilder.build()).build();
        return this.call(request, type);
    }

    public <T> T post(String path, Object object, Type type) throws IOException, GoxyApiErrorException
    {
        String json = object != null ? this.objectMapper.writeValueAsString(object) : "";
        RequestBody body = RequestBody.create(json, JSON);

        Request request = this.buildRequest().url(this.baseUrl + path).post(body).build();
        return this.call(request, type);
    }

    public <T> T patch(String path, Object object, Type type) throws IOException, GoxyApiErrorException
    {
        String json = object != null ? this.objectMapper.writeValueAsString(object) : "";
        RequestBody body = RequestBody.create(json, JSON);

        Request request = this.buildRequest().url(this.baseUrl + path).patch(body).build();
        return this.call(request, type);
    }

    public <T> T delete(String path, Object object, Type type) throws IOException, GoxyApiErrorException
    {
        String json = object != null ? this.objectMapper.writeValueAsString(object) : "";
        RequestBody body = RequestBody.create(json, JSON);

        Request request = this.buildRequest().url(this.baseUrl + path).delete(body).build();
        return this.call(request, type);
    }
}
