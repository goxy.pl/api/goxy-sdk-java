package pl.goxy.sdk;

import com.fasterxml.jackson.core.type.TypeReference;
import org.jetbrains.annotations.NotNull;
import pl.goxy.sdk.exception.GoxyApiErrorException;
import pl.goxy.sdk.model.GoxyProfile;
import pl.goxy.sdk.model.request.ProfilePrioritiesUpdateForm;
import pl.goxy.sdk.model.request.ProfileUpdatePasswordForm;
import pl.goxy.sdk.model.request.ServerCreateLegacyRequest;
import pl.goxy.sdk.model.request.ServerCreateRequest;
import pl.goxy.sdk.model.response.ContainerResponse;
import pl.goxy.sdk.model.response.ServerResponse;
import pl.goxy.sdk.model.response.TokenResponse;
import pl.goxy.sdk.model.response.TokenResponse.TokenType;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GoxyClient
        implements GoxySdk {
    private final HttpRequest request;

    public GoxyClient(TokenType tokenType, String token, GoxyProperties properties) {
        this.request = new HttpRequest(API_BASE_URL, tokenType, token, properties);
    }

    @Override
    public TokenResponse getTokenInfo() throws IOException, GoxyApiErrorException {
        return this.request.get("/token", TokenResponse.class);
    }

    @Override
    public List<ContainerResponse> getContainers() throws IOException, GoxyApiErrorException {
        Type type = new TypeReference<List<ContainerResponse>>() {
        }.getType();
        return this.request.get("/container", type);
    }

    @Override
    public ServerResponse getServer(@NotNull UUID id) throws IOException, GoxyApiErrorException {
        return this.request.get("/server/" + id, ServerResponse.class);
    }

    @Override
    public ServerResponse createServer(@NotNull ServerCreateRequest request) throws IOException, GoxyApiErrorException {
        return this.request.post("/server", request, ServerResponse.class);
    }

    @Override
    public ServerResponse createServer(@NotNull ServerCreateLegacyRequest request) throws IOException, GoxyApiErrorException {
        return this.request.post("/server", request, ServerResponse.class);
    }

    @Override
    public void deleteServer(@NotNull UUID serverId) throws IOException, GoxyApiErrorException {
        this.request.delete("/server/" + serverId, null, Void.class);
    }

    @Override
    public void changeProfilePassword(@NotNull String name, ProfileUpdatePasswordForm form) throws IOException, GoxyApiErrorException {
        this.request.patch("/server/profile/" + name, form, Void.class);
    }

    @Override
    public void unregisterProfile(@NotNull String name) throws IOException, GoxyApiErrorException {
        this.request.delete("/server/profile/" + name, null, Void.class);
    }

    @Override
    public Optional<GoxyProfile> getProfile(@NotNull String name) throws IOException, GoxyApiErrorException {
        return Optional.ofNullable(this.request.get("/users/profiles/minecraft/" + name, GoxyProfile.class));
    }

    @Override
    public void updateProfilePriorities(ProfilePrioritiesUpdateForm form) throws IOException, GoxyApiErrorException {
        this.request.post("/server/profile/priority", form, Void.class);
    }
}