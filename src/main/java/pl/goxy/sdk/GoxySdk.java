package pl.goxy.sdk;

import pl.goxy.sdk.exception.GoxyApiErrorException;
import pl.goxy.sdk.model.GoxyProfile;
import pl.goxy.sdk.model.request.ProfilePrioritiesUpdateForm;
import pl.goxy.sdk.model.request.ProfileUpdatePasswordForm;
import pl.goxy.sdk.model.request.ServerCreateLegacyRequest;
import pl.goxy.sdk.model.request.ServerCreateRequest;
import pl.goxy.sdk.model.response.ContainerResponse;
import pl.goxy.sdk.model.response.ServerResponse;
import pl.goxy.sdk.model.response.TokenResponse;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GoxySdk
{
    String API_BASE_URL = "https://api.goxy.pl";

    TokenResponse getTokenInfo() throws IOException, GoxyApiErrorException;

    List<ContainerResponse> getContainers() throws IOException, GoxyApiErrorException;

    ServerResponse getServer(UUID id) throws IOException, GoxyApiErrorException;

    ServerResponse createServer(ServerCreateRequest request) throws IOException, GoxyApiErrorException;

    ServerResponse createServer(ServerCreateLegacyRequest request) throws IOException, GoxyApiErrorException;

    void deleteServer(UUID serverId) throws IOException, GoxyApiErrorException;

    void changeProfilePassword(String name, ProfileUpdatePasswordForm form) throws IOException, GoxyApiErrorException;

    void unregisterProfile(String name) throws IOException, GoxyApiErrorException;

    Optional<GoxyProfile> getProfile(String name) throws IOException, GoxyApiErrorException;

    void updateProfilePriorities(ProfilePrioritiesUpdateForm form) throws IOException, GoxyApiErrorException;
}
