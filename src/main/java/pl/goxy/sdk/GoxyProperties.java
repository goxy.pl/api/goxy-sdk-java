package pl.goxy.sdk;

public interface GoxyProperties
{
    String getName();

    String getVersion();
}
